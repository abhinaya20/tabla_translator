# README #

This repository contains the source code for the translator that generates a Caffe2 equivalent of a Tabla source code.  
The repository is organized as follows:
	
	* The root folder contains folder for Tabla's translators to different framework. Currently, only Caffe2 is supported  
	* Inside each framework's sub-folder, there are two sub-folders: src and tabla_examples.  
	* src contains the source code for the translator. It also contains a sub-folder consisting of the modification in Tabla  
		required by the translator for efficient code generation  
	* tabla_examples sub-folder consists of some example algorithms coded in tabla. Each sub-folder's name indicates the algorithm  
		being worked upon. Inside this sub-folder,
		* the artifacts sub-folder contains tabla source for the algorithm and the intermediate files generated upon compilation  
		* the outputs sub-folder contains the output of the translator for this algorithm  
			