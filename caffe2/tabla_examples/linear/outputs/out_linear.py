#! /usr/bin/env python

import sys
from caffe2.python import workspace
from caffe2.python import model_helper
from caffe2.python import core
from caffe2.python import net_drawer
from caffe2.python import brew
import numpy as np

# class Trainer. Contains definitions for initialization and creation of the training net

class Trainer:
	def __init__(self, mu, m, x, y):
		# Inputs, constants and thier values as translated from the Tabla source
		self.train_net = model_helper.ModelHelper("train_net")
		self.mu = self.train_net.param_init_net.ConstantFill( [], "mu", shape=[1,1], value=mu )
		self.m = self.train_net.param_init_net.ConstantFill( [], "m", shape=[1,1], value=m )
		self._x = x
		self._y = y
		self.w = self.train_net.ConstantFill( [], "w", shape=[ 3], value=0.0 )
		# Auxillary inputs. The numpy inputs are coverted into Blob accepted by the Caffe2 Nets
		self.x_0 = self.train_net.param_init_net.ConstantFill( [], "x_0", shape=[1,1], value=self._x[0, 0] )
		self.x_1 = self.train_net.param_init_net.ConstantFill( [], "x_1", shape=[1,1], value=self._x[0, 1] )
		self.x_2 = self.train_net.param_init_net.ConstantFill( [], "x_2", shape=[1,1], value=self._x[0, 2] )
		self.y = self.train_net.param_init_net.ConstantFill( [], "y", shape=[1,1], value=self._y[0] )
		self.w_0 = self.train_net.ConstantFill( [], "w_0", shape=[1,1], value=0.0 )
		self.w_1 = self.train_net.ConstantFill( [], "w_1", shape=[1,1], value=0.0 )
		self.w_2 = self.train_net.ConstantFill( [], "w_2", shape=[1,1], value=0.0 )
		workspace.RunNetOnce(self.train_net.param_init_net)

	# 'model_definition' function adds operations for both forward and backwards pass to the model
	def model_definition(self):
		out_node_11 = self.train_net.MatMul([self.w_1, self.x_1], "out_node_11")
		out_node_10 = self.train_net.MatMul([self.w_0, self.x_0], "out_node_10")
		out_node_12 = self.train_net.MatMul([self.w_2, self.x_2], "out_node_12")
		out_node_13 = self.train_net.Add([out_node_10, out_node_11], "out_node_13")
		out_node_14 = self.train_net.Add([out_node_13, out_node_12], "out_node_14")
		out_node_15 = self.train_net.Sub([out_node_14, self.y], "out_node_15")
		out_node_18 = self.train_net.MatMul([out_node_15, self.x_2], "out_node_18")
		out_node_17 = self.train_net.MatMul([out_node_15, self.x_1], "out_node_17")
		out_node_16 = self.train_net.MatMul([out_node_15, self.x_0], "out_node_16")
		out_node_23 = self.train_net.MatMul([self.mu, out_node_18], "out_node_23")
		out_node_21 = self.train_net.MatMul([self.mu, out_node_17], "out_node_21")
		out_node_19 = self.train_net.MatMul([self.mu, out_node_16], "out_node_19")
		self.train_net.Sub([self.w_2, out_node_23], "w_2")
		self.train_net.Sub([self.w_1, out_node_21], "w_1")
		self.train_net.Sub([self.w_0, out_node_19], "w_0")

	# 'create_net' function initializes the model and runs the training model
	def model_create(self, it):
		workspace.CreateNet(self.train_net.net, overwrite=True)
		for i in range (0, it):
			for j in range (0, self._x.shape[0]):
				self.x_0 = self.train_net.param_init_net.ConstantFill( [], "x_0", shape=[1,1], value=self._x[j, 0] )
				self.x_1 = self.train_net.param_init_net.ConstantFill( [], "x_1", shape=[1,1], value=self._x[j, 1] )
				self.x_2 = self.train_net.param_init_net.ConstantFill( [], "x_2", shape=[1,1], value=self._x[j, 2] )
				self.y = self.train_net.param_init_net.ConstantFill( [], "y", shape=[1,1], value=self._y[j] )
				workspace.RunNetOnce(self.train_net.param_init_net)
				workspace.RunNet(self.train_net.net)

def add_input(file_name, which_cols):
	l = []
	for i in which_cols.split(','):
		l.append(int(i))
	return np.loadtxt(fname=file_name, delimiter=',', usecols=l)

# Execution starts here
def main(argv):
	fd_config = open("config.txt", "r")
	mu = 1.0
	m = 3.0

	x_file = [line for line in fd_config.read().splitlines() if 'cfg_x.file' in line][0].split(' ')[1]
	fd_config.seek(0)
	x_cols = [line for line in fd_config.read().splitlines() if 'cfg_x.cols' in line][0].split(' ')[1]
	fd_config.seek(0)
	x = add_input(x_file, x_cols)
	print(x)


	y_file = [line for line in fd_config.read().splitlines() if 'cfg_y.file' in line][0].split(' ')[1]
	fd_config.seek(0)
	y_cols = [line for line in fd_config.read().splitlines() if 'cfg_y.cols' in line][0].split(' ')[1]
	fd_config.seek(0)
	y = add_input(y_file, y_cols)
	print(y)

	trainer = Trainer(mu, m, x, y)
	trainer.model_definition()

	it = 1
	trainer.model_create(it)
	for name in workspace.Blobs():
		print("{}:\n{}".format(name, workspace.FetchBlob(name)))
	

main(sys.argv)
