#! /usr/bin/env python

import sys
import re
import operator

class NumpyTemplates:
   def __init__(self, fd):
      self.fd = fd
      self.string_import_header = "import numpy as np" # numpy import header. Used to generate random inputs and fills
      self.string_random_tensor = "{vname} = np.random.rand({vdim}).astype({vtype})" # numpy random fills
      self.string_empty_tensor = "{vname} = np.empty({vdim})" # numpy generate empty array
      self.string_const_tensor = "{vname} = np.full({vdim}, {init_val}, dtype={vtype})" # numpy generate constant filled array
      self.string_load_text = "np.loadtxt(fname={file_name}, delimiter={delim}, usecols={which_cols})"

      
   def add_header(self):
      self.fd.write(self.string_import_header + "\n")

   def add_rand_tensor(self, vname, vdim, vtype):
      self.fd.write(self.string_random_tensor.format(vname=vname, vdim=vdim, vtype=vtype) + "\n")
      
   def add_const_tensor(self, vname, vdim):
      self.fd.write(self.string_const_tensor.format(vname=vname, vdim=vdim) + "\n")
   
   def get_rand_tensor(self, vname, vdim, vtype):
      return self.string_random_tensor.format(vname=vname, vdim=vdim, vtype=vtype) + "\n"
   
   def get_const_tensor(self, vname, vdim, val, vtype):
      return self.string_const_tensor.format(vname=vname, vdim=vdim, init_val=val, vtype=vtype) + "\n"
   
   def get_loadtext(self, file_name, which_cols):
	   return self.string_load_text.format(file_name=file_name, delim='\',\'', which_cols=which_cols)


class Caffe2Templates:
   def __init__(self, fd):
      self.fd = fd
      self.string_python_env = "#! /usr/bin/env python" # caffe2 python env
      self.string_import = "from caffe2.python import " # caffe2 import
      self.string_feedblob = "workspace.FeedBlob(\"{vname}\", {vname})"# caffe2 feed input template

      #self.string_model_helper_init = "{vname} = model_helper.ModelHelper(name=\"{vname}\")" # caffe2 model_helper
      #self.string_constant_fill = '{tensor_name} = self.{model_name}.ConstantFill( [], "{tensor_name}", shape={dim_list}, value={init_val} )'
      #self.string_given_tensor_fill = '{tensor_name} = self.{model_name}.GivenTensorFill( [], "{tensor_name}", shape={dim_list}, values={init_list} )'
      #self.string_param_init = '.param_init_net'
      #self.string_function_call = 'self.{model_name}.{opcode}([{input_list}], "{output_name}")'
      #self.string_model_init = 'workspace.RunNetOnce(self.{model_name}.param_init_net)'
      #self.string_model_create = 'workspace.CreateNet(self.{model_name}, overwrite={overwrite})'
      #self.string_model_run = 'workspace.RunNet(self.{model_name})'

      self.string_param_init = 'param_init_net'
      self.string_model_helper_init = "{vname} = model_helper.ModelHelper(\"{vname}\")" # caffe2 model_helper
      self.string_constant_fill = '{tensor_name} = self.{model_name}.ConstantFill( [], "{tensor_name}", shape={dim_list}, value={init_val} )'
      self.string_given_tensor_fill = '{tensor_name} = self.{model_name}.GivenTensorFill( [], "{tensor_name}", shape={dim_list}, values={init_list} )'
      self.string_function_call = 'self.{model_name}.{opcode}([{input_list}], "{output_name}")'
      self.string_model_init = 'workspace.RunNetOnce(self.{model_name}.{net})'
      self.string_model_create = 'workspace.CreateNet(self.{model_name}.net, overwrite={overwrite})'
      self.string_model_run = 'workspace.RunNet(self.{model_name}.net)'
      self.string_print_all_blobs = '''for name in workspace.Blobs():\n\t\tprint("{}:\\n{}".format(name, workspace.FetchBlob(name)))\n'''

      self.optype_map = {'Add': 2, 'Sub': 2, 'MatMul': 2, 'Sigmoid': 1}

   def reset_file_pointer(self)   :
      fd.seek(0)

   def get_optype(self, opcode):
      return self.optype_map[opcode]

   def add_python_env(self):
      self.fd.write(self.string_python_env + "\n\n")
      
   def add_header(self, module):
      self.fd.write(self.string_import + module + "\n")

   def add_feedblob(self, vname, vdim, vtype):
      self.fd.write(self.string_feedblob.format(vname=vname) + "\n")
   
   def add_model_helper(self, vname):
      self.fd.write(self.string_model_helper_init.format(vname=vname) + "\n")
   
   def add_text(self, text):
      self.fd.write(text)
   
   def add_comment(self, comment):
      self.fd.write("\n# " + comment + "\n")
   
   def get_feedblob_string(self, vname, vdim, vtype):
      return (self.string_feedblob.format(vname=vname) + "\n")
      
   def get_model_init_string(self, vname):
      return (self.string_model_helper_init.format(vname=vname) + "\n")

   def get_fill_constant_string(self, tensor_name, model_name, dim_list, init_val, run_once):
      s = model_name + '.' +  self.string_param_init if run_once == 1 else model_name
      init_val = 'float(' + str(init_val) + ')'
      return self.string_constant_fill.format(tensor_name=tensor_name, model_name=s, dim_list=dim_list, init_val=init_val) + "\n"

   def get_fill_given_string(self, tensor_name, model_name, dim_list, init_list, run_once):
      s = model_name + self.string_param_init if run_once == 1 else model_name
      return self.string_given_tensor_fill.format(tensor_name=tensor_name, model_name=s, dim_list=dim_list, init_list=init_list) + "\n"

   def get_function_call_string(self, model_name, opcode, src, dest, explicit):
      optype = self.get_optype(opcode)
      input_list = ''
      if optype == 1:
         assert len(src) == 1
         input_list = src[0]
      elif optype == 2:
         assert len(src) == 2
         input_list = src[0] + ', ' + src[1]
      func = self.string_function_call.format(model_name=model_name, opcode=opcode, input_list=input_list, output_name=dest)
      if explicit == 1:
         func = dest + ' = ' + func
      return func

   def get_model_helper_string(self, model_name):
      return self.string_model_helper_init.format(vname=model_name) + '\n'
   
   def get_model_init_string(self, model_name):
      return self.string_model_init.format(model_name=model_name, net=self.string_param_init) + '\n'

   def get_model_create_string(self, model_name, overwrite):
      return self.string_model_create.format(model_name=model_name, overwrite=overwrite) + '\n'

   def get_iter_run_net_string(self, model_name):
      return self.string_model_run.format(model_name=model_name) + '\n'

   def print_all_blobs(self):
      return self.string_print_all_blobs;
